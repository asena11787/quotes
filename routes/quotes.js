var express = require('express');
var router  = express.Router();
var Quote   = require('../app/models/quote');

// create a quote (accessed at POST http://localhost:8080/api/quote)
router.post(function(req, res) {
    
    var quote = new Quote();      // create a new instance of the Quote model
    quote.quote   = req.body.quote;  // set the quote text
    quote.author  = req.body.author;
    quote.subject = req.body.subject;

    // save the bear and check for errors
    quote.save(function(err) {
        if (err)
            res.send(err);

        res.json({ message: 'Quote created!' });
    });
});

// get all the quotes (accessed at GET http://localhost:8080/api/quotes)
router.get('/', function(req, res) {
    console.log('got here');
    Quote.find(function(err, quotes) {
        if (err)
            res.send(err);

        // res.json(quotes);
        res.render('index', {
        	quotes: quotes,
        	title: 'Hello Jerry!'
        });
    });
});

// on routes that end in /quotes/:quote_id
// ----------------------------------------------------
router.route('/quotes/:quote_id')

// get the quote with that id (accessed at GET http://localhost:8080/api/quotes/:quote_id)
router.get(function(req, res) {
    Quote.findById(req.params.quote_id, function(err, quote) {
        if (err)
            res.send(err);
        res.json(quote);
    });
});

// update the quote with this id (accessed at PUT http://localhost:8080/api/quotes/:quote_id)
router.put(function(req, res) {

    // use our quote model to find the quote we want
    Quote.findById(req.params.quote_id, function(err, quote) {

        if (err)
            res.send(err);

        // update the quotes info
        quote.quote   = req.body.quote;
        quote.author  = req.body.author;
        quote.subject = req.body.subject;

        // save the bear
        quote.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Quote updated!' });
        });

    });
});

// delete the quote with this id (accessed at DELETE http://localhost:8080/api/quotes/:quote_id)
router.delete(function(req, res) {
    Quote.remove({
        _id: req.params.quote_id
    }, function(err, quote) {
        if (err)
            res.send(err);

        res.json({ message: 'Successfully deleted' });
    });
});

module.exports = router;