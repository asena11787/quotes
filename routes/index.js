module.exports = function (app) {
	var express = require('express');
	var router  = express.Router();

	// middleware to use for all requests
	router.use(function(req, res, next) {
		// do logging
		console.log('Something is happening.');
		next(); // make sure we go to the next routes and don't stop here
	});

    app.use('/quotes', require('./quotes'));
};