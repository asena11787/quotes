// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express		= require('express');		// call express
var app			= express();				// define our app using express
var bodyParser	= require('body-parser');
var mongoose	= require('mongoose');

//mongoose.connect('mongodb://asena11787:Sh@w@ngunks1@ds056698.mongolab.com:56698/quotes'); // connect to our database

mongoose.connect('mongodb://localhost/quotes'); // connect to our database
var path        = require('path');

// Set the  view enjine to EJS
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// configure app to use bodyParser()
// this will let us get the data from a post
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;		//set our port

//// ROUTES FOR OUR API
//// =================================================================================
//var router = express.Router();				// get an instance of the express Router

//// middleware to use for all requests
//router.use(function(req, res, next) {
//	// do logging
//	console.log('Something is happening.');
//	next(); // make sure we go to the next routes and don't stop here
//});

// test route to make sure everything is working
// accessed at GET http://localhost:8080/api
//router.get('/', function(req, res) {
//	res.render('index', { title: 'hooray! welcome to our api!'});
//});

// more routes for our api will happen here

require('./routes/index')(app);

// START THE server
// ================================================================================
app.listen(port);
console.log('Magic happens on port ' + port);